#!/bin/sh

apt update
apt install -y doxygen
pip3 install jinja2
pip3 install pygments

git clone https://gitlab.com/bp3d/framework/core

#Prepare Framework
#cp base.html Framework
#cp doxygen.py Framework
cp index.html core
cp Doxyfile DoxyfileMCSS core
cp logo.png logo_white.ico core
cd core
git clone https://github.com/mosra/m.css.git

#mv base.html ./m.css/documentation/templates/doxygen
#mv doxygen.py ./m.css/documentation
cd m.css/documentation && python3 doxygen.py ../../DoxyfileMCSS && cd ../../
mv index.html ./html
mv ./html ../public
cd ..
